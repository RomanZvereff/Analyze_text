package com.zrv.analyzetext;

import java.io.IOException;
import java.util.*;

public class Manipulations {

    ReadFromFile readFromFile = new ReadFromFile();

    public void countSameWords(List <String> list) throws IOException {

        String result = String.join(" ", list);
        String splitResult[] = result.split("[ .,]");
        HashMap<String, Integer> uniques = new HashMap<>();

        for (String word : splitResult) {
            Integer count = uniques.get(word);
            uniques.put(word, (count == null ? 1 : (count + 1)));
        }

        Set<Map.Entry<String, Integer>> uniqueSet = uniques.entrySet();
        for (Map.Entry<String, Integer> entry : uniqueSet) {
            if (entry.getValue() > 1) {
                System.out.println(entry.getKey() + " => " + entry.getValue());
            }
        }System.out.println("<===========================>");
    }

    public void sortWords() throws IOException {

        String result = String.join(" ", readFromFile.readText());
        String splitResult[] = result.split("[ .,]");

        Arrays.sort(splitResult);
        for (int i = 0; i < splitResult.length; i++) {
            System.out.println(splitResult[i]);
        }System.out.println("<===========================>");
    }

    public String removeShortWord(List<String> list) throws IOException {

        String result = String.join(" ", list);
        String splitResult[] = result.split("[ .,]");

        int minLength = 3;
        String resultArray = "";

        for (int i = 0; i < splitResult.length; i++) {
            String word = splitResult[i];
            if (word.length() >= minLength) {
                resultArray += word + " ";
            }
        }return resultArray;
    }
}