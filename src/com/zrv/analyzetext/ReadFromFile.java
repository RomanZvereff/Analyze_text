package com.zrv.analyzetext;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ReadFromFile {

    public List readText() throws IOException {

        List<String> text = Files.readAllLines(Paths.get("whatIsJava.txt"));
        return text;
    }
}