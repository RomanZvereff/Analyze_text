package com.zrv.analyzetext;

import java.io.IOException;
import java.util.Scanner;

public class Menu {


    public static void drawMenu() throws IOException {

        Manipulations manipulations = new Manipulations();

        Scanner scanner = new Scanner(System.in);

        int userAnswer = 0;
        int answer = 9;

        while (userAnswer != answer) {

            ReadFromFile readFromFile = new ReadFromFile();
            System.out.println(readFromFile.readText());
            System.out.println("<===========================>");
            final String[] MENU = {
                    "What do you want to do?",
                    "To count occurrence of same words in collection write - 1",
                    "To sort words alphabetically write - 2",
                    "To remove words with length < = 3 write - 3",
                    "Write \"0\" to EXIT",
                    "<===========================>"
            };

            for (int i = 0; i < MENU.length; i++) {
                System.out.println(MENU[i]);
            }

            System.out.println("Write your choice:");
            answer = scanner.nextInt();

            if (answer == 1) {
                manipulations.countSameWords(readFromFile.readText());
            }
            if (answer == 2) {
                manipulations.sortWords();
            }
            if (answer == 3) {
                System.out.println(manipulations.removeShortWord(readFromFile.readText()));
                System.out.println("<===========================>");
            }
            if (answer == 0){
                System.out.println("EXIT!");
            }
        }
    }
}