package com.zrv.analyzetext;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)

public class TestAnalyzeText {

    @Test
    public void testRemove() throws IOException {

        Manipulations manipulations = new Manipulations();

        List<String> testlist = new ArrayList<>();
        testlist.add("qwerty");
        testlist.add("abc");
        testlist.add("thisIsManySymbols");
        String string = "qwertyt hisIsManySymbols";

        Assert.assertEquals(string, manipulations.removeShortWord(testlist));
    }
}